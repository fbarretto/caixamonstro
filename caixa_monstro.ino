#include <Adafruit_NeoPixel.h>

#define SENSOR 2
#define LED_PIN 4
#define LED_COUNT 7

int brightness = 255;
int currentState = 0;
int lastState = 0;

Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  // make the sensor's pin an input:
  pinMode(SENSOR, INPUT);
  strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.show();            // Turn OFF all pixels ASAP
}

void loop() {
  // read the sensor pin:
  currentState = digitalRead(SENSOR);
  // print out the state of the sensor:
  Serial.println(currentState);
  if (currentState == 1 && lastState == 0) {
    colorWipe(strip.Color(206,   40,   90)     , 280); // Red
    colorWipe(strip.Color(  224, 86, 32  )     , 280); // Green
    colorWipe(strip.Color(  172,   113, 255)     , 280); // Blue
    colorWipe(strip.Color(  82,   167, 255)     , 280); // Blue  
    colorWipe(strip.Color(  245,   245, 245)     , 280); // Blue
    colorWipe(strip.Color(  0,   0, 0)     , 280); // Blue
  }
  
  lastState = currentState;
  
  delay(10);        // delay in between reads for stability
}

// Fill strip pixels one after another with a color. Strip is NOT cleared
// first; anything there will be covered pixel by pixel. Pass in color
// (as a single 'packed' 32-bit value, which you can get by calling
// strip.Color(red, green, blue) as shown in the loop() function above),
// and a delay time (in milliseconds) between pixels.
void colorWipe(uint32_t color, int wait) {
  for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
    strip.setPixelColor(i, color);         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match
    delay(wait);                           //  Pause for a moment
  }
}
